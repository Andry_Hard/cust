# -*- coding: utf-8 -*-
import string
import random
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.conf import settings
from .fields import PhoneNumberField

__all__=("MyUser", "MyUserAccount")

class MyUserManager(BaseUserManager):

    def rand_password(size=6, chars=string.ascii_lowercase + string.digits):
    	return ''.join(random.choice(chars) for _ in range(size))

    def create_user(self, username, email=None, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not username:
            raise ValueError('Users must have an email address')
	
        user = self.model(
            username=self.normalize_email(username),
        )
	
	if not email:
	    user.email=self.normalize_email(username)

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, nik, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(username,
            password=password,
            email=self.normalize_email(username),
	    nik=nik,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user
    
class MyUser(AbstractBaseUser, PermissionsMixin):
    username = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
        db_index=True,
    )
    email=models.CharField(max_length=250, null=True)
    tmp_pass=models.CharField(max_length=30, null=True)
    nik = models.CharField(max_length=50, null=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = MyUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['nik','tmp_pass','email']

    def get_full_name(self):
        # The user is identified by their email address
        return self.nik

    def get_short_name(self):
        # The user is identified by their email address
        return self.nik

    # On Python 3: def __str__(self):
    def __unicode__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

class MyUserAccount(models.Model):
	u=models.OneToOneField(settings.AUTH_USER_MODEL)
	imia=models.CharField(max_length=20, null=True, blank=True)
	prizv=models.CharField(max_length=20, null=True, blank=True)
	phone=PhoneNumberField()
	ava=models.ImageField(upload_to="avatars", default='img/thanks.jpg')

	class Meta:
		verbose_name="профіль"
		verbose_name_plural="Профілі"

	def full_name(self):
            if self.imia and self.prizv:
                return "%s %s"%(self.imia, self.prizv)
            else:
                return self.u.get_full_name()

	def __unicode__(self):
		return self.full_name()

class ContactInfo(models.Model):
    username=models.CharField(max_length=250)
    cust=models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)
    email=models.EmailField()
    phone=PhoneNumberField()
    date_time=models.DateTimeField(auto_now_add=True)
    class Meta:
        abstract=True    
