from django.db.models import fields
import re

from django.forms.fields import CharField
from django.core.validators import EMPTY_VALUES
from django.forms import ValidationError
from django.utils.translation import ugettext_lazy as _

phone_digits = re.compile(r'^(\+38)?\(?0\d{2}\)?[-\s]?\d{3}([-\s]\d{2}){2}$')


class UAPhoneNumberField(CharField):
    default_error_messages = {
        'invalid': _('Phone numbers must be in +38(0XX)XXX-XX-XX,\
         0XX-XXX-XX-XX, or (0XX) XXX-XX-XX format.'),
    }

    def clean(self, value):
        super(UAPhoneNumberField, self).clean(value)
        if value in EMPTY_VALUES:
            return u''
        match = phone_digits.match(value)
        if match:
            return value
        raise ValidationError(self.error_messages['invalid'])
        
class PhoneNumberField(fields.CharField):

    description = _("Ukrainian phone number")

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 17
        super(PhoneNumberField, self).__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'form_class': UAPhoneNumberField}
        defaults.update(kwargs)
        return super(PhoneNumberField, self).formfield(**defaults)